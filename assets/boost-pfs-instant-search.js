// Override Settings
var boostPFSInstantSearchConfig = {
	search: {
		suggestionPosition: 'right',
		suggestionMobileStyle: 'style2',
	}
};

(function () {  // Add this

	BoostPFS.inject(this);  // Add this
	// Customize style of Suggestion box
	SearchInput.prototype.customizeInstantSearch = function(suggestionElement, searchElement, searchBoxId) {
		if (jQ(searchBoxId).closest('.search_container').length > 0) {
			this.setSuggestionWidth(searchBoxId, 400);
		}
	};

})();  // Add this at the end