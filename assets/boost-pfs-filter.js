// Override Settings
var boostPFSFilterConfig = {
	general: {
		limit: boostPFSConfig.custom.products_per_page,
		// Optional
		loadProductFirst: true,
		// paginationType: boostPFSConfig.custom.pagination_type == 'infinite_scroll' ? 'infinite' :
		// 	(boostPFSConfig.custom.pagination_type == 'load_more' ? 'load_more' : 'default'),
		filterTreeMobileStyle: 'style1'
	},
	template: {
		loadMoreLoading: '<div class="boost-pfs-filter-load-more-loading"><div class="load-more__icon" style="width: 44px; height: 44px; opacity: 1;"></div></div>',
	},
	selector: {
		breadcrumb: '.breadcrumb-collection'
	}
};

// Declare Templates
var boostPFSTemplate = {
	'saleLabelHtml': '<div class="sale_banner thumbnail_banner">' + boostPFSConfig.label.sale + '</div>',
	'newLabelHtml': '<div class="new_banner thumbnail_banner">' + boostPFSConfig.label.new + '</div>',
	'preorderLabelHtml': '<div class="new_banner thumbnail_banner">' + boostPFSConfig.label.pre_order + '</div>',
	'quickViewBtnHtml': '<span data-fancybox-href="#product-{{itemId}}" class="quick_shop ss-icon" data-gallery="product-{{itemId}}-gallery">&#x002B;</span>',
	'newRowHtml': '<br class="clear product_clear" />',

	// Grid Template
	'productGridItemHtml': 	'<div class="{{flipImageClass}} {{itemColumnNumberClass}} {{itemCollectionGroupThumbClass}} thumbnail {{itemCollectionGroupSmallClass}} quick-shop-style--'+ boostPFSConfig.custom.quick_shop_style +' product-{{itemId}}" data-boost-theme-quickview="{{itemId}}">' +
									'<div class="product-wrap">' +
										'<div class="relative product_image swap-' + boostPFSConfig.custom.secondary_image + '">' +
											'<a href="{{itemUrl}}">' +
												'{{itemImages}}' +
											'</a>' +
											'{{itemColorSwatchesInline}}' +
											'{{itemProductInfoHover}}' +
											'<div class="banner_holder">' +
												'{{itemSaleLabel}}' +
												'{{itemNewLabel}}' +
												'{{itemPreorderLabel}}' +
											'</div>' +
										'</div>' +
										'<a class="product-info__caption {{itemHiddenClass}}" href="{{itemUrl}}">' +
											'{{itemProductInfo}}' +
										'</a>' +
									'</div>' +
									'{{itemColorSwatchesPopup}}' +
								'</div>',


	// Pagination Template
	'previousHtml': '<span class="prev"><a href="{{itemUrl}}">« ' + boostPFSConfig.label.paginate_prev + '</a></span>',
	'nextHtml': '<span class="next"><a href="{{itemUrl}}">' + boostPFSConfig.label.paginate_next + ' »</a></span>',
	'pageItemHtml': '<span class="page"><a href="{{itemUrl}}">{{itemTitle}}</a></span>',
	'pageItemSelectedHtml': '<span class="page current">{{itemTitle}}</span>',
	'pageItemRemainHtml': '<span>{{itemTitle}}</span>',
	'paginateHtml': '{{previous}}{{pageItems}}{{next}}',

	// Sorting Template
	'sortingHtml': '{{sortingItems}}',
};


(function () { // Add this
	BoostPFS.inject(this); // Add this
	
	// Build Product Grid Item
	ProductGridItem.prototype.compileTemplate = function (data, index) {
		if(!data) data = this.data;
		if (!index) index = this.index + 1; 

		/*** Prepare data ***/
		var images = data.images_info;
		if (images.length > 0) {
		data.featured_image = images[0];
		} else {
			data.featured_image = {
				'height': 1,
				'width': 1
			}
		}
		// Displaying price base on the policy of Shopify, have to multiple by 100
		var soldOut = !data.available; // Check a product is out of stock
		var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
		var priceVaries = data.price_min != data.price_max; // Check a product has many prices
		// Get First Variant (selected_or_first_available_variant)
		var firstVariant = data['variants'][0];
		if (Utils.getParam('variant') !== null && Utils.getParam('variant') != '') {
			var paramVariant = data.variants.filter(function (e) {
				return e.id == Utils.getParam('variant');
			});
			if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0];
		} else {
			for (var i = 0; i < data['variants'].length; i++) {
				if (data['variants'][i].available) {
					firstVariant = data['variants'][i];
					break;
				}
			}
		}
		/*** End Prepare data ***/

		// Get Template
		var itemHtml = boostPFSTemplate.productGridItemHtml;

		var onSaleClass = onSale ? 'sale' : '';
		var soldOutClass = soldOut ? 'out_of_stock' : 'in_stock';
		var availabilityProp = soldOut ? 'http://schema.org/SoldOut' : 'http://schema.org/InStock';

		// Add custom class
		var itemColumnNumberClass = '';
		var itemCollectionGroupThumbClass = buildItemCollectionGroupThumbClass(index, boostPFSConfig.custom.products_per_row);
		var itemCollectionGroupLargeClass = '';
		var itemCollectionGroupMediumClass = '';
		var itemCollectionGroupSmallClass = (index - 1) % 2 == 0 ? 'even' : 'odd';

		switch (boostPFSConfig.custom.products_per_row) {
			case 2:
				itemColumnNumberClass = 'eight columns';
				break;
			case 3:
				itemColumnNumberClass = 'one-third column';
				break;
			case 4:
				itemColumnNumberClass = 'four columns';
				break;
			case 5:
				itemColumnNumberClass = 'one-fifth column';
				break;
			case 6:
				itemColumnNumberClass = 'one-sixth column';
				break;
			default:
				itemColumnNumberClass = 'one-seventh column';
				break;
		}

		if (boostPFSConfig.custom.mobile_products_per_row == 1) {
			itemColumnNumberClass += ' medium-down--one-half small-down--one-whole';
		} else {
			itemColumnNumberClass += ' medium-down--one-half small-down--one-half';
		}

		itemHtml = itemHtml.replace(/{{itemColumnNumberClass}}/g, itemColumnNumberClass);
		itemHtml = itemHtml.replace(/{{itemCollectionGroupThumbClass}}/g, itemCollectionGroupThumbClass);
		itemHtml = itemHtml.replace(/{{itemCollectionGroupLargeClass}}/g, itemCollectionGroupLargeClass);
		itemHtml = itemHtml.replace(/{{itemCollectionGroupMediumClass}}/g, itemCollectionGroupMediumClass);
		itemHtml = itemHtml.replace(/{{itemCollectionGroupSmallClass}}/g, itemCollectionGroupSmallClass);

		// Add soldOut label
		var itemSoldOutLabel = soldOut ? boostPFSTemplate.soldOutLabelHtml : '';
		itemHtml = itemHtml.replace(/{{itemSoldOutLabel}}/g, itemSoldOutLabel);

		// Add onSale label
		var itemSaleLabel = boostPFSConfig.custom.sale_banner_enabled && onSale ? boostPFSTemplate.saleLabelHtml : '';
		itemHtml = itemHtml.replace(/{{itemSaleLabel}}/g, itemSaleLabel);

		// Add Label (New, Coming soon, Pre order)
		var newLabel = data.collections.filter(function (e) {
			return e.handle == 'new';
		});
		var preorderLabel = data.collections.filter(function (e) {
			return e.handle == 'pre-order';
		});
		var comingsoonLabel = data.collections.filter(function (e) {
			return e.handle == 'coming-soon';
		});
		if (data.collections) {
			var itemNewLabelHtml = typeof newLabel[0] != 'undefined' ? boostPFSTemplate.newLabelHtml : '';
			itemHtml = itemHtml.replace(/{{itemNewLabel}}/g, itemNewLabelHtml);

			var itemComingsoonLabelHtml = typeof comingsoonLabel[0] != 'undefined' ? boostPFSTemplate.comingsoonLabelHtml : '';
			itemHtml = itemHtml.replace(/{{itemComingsoonLabel}}/g, itemComingsoonLabelHtml);

			var itemPreorderLabelHtml = typeof preorderLabel[0] != 'undefined' ? boostPFSTemplate.preorderLabelHtml : '';
			itemHtml = itemHtml.replace(/{{itemPreorderLabel}}/g, itemPreorderLabelHtml);
		}

		var itemHiddenClass = boostPFSConfig.custom.thumbnail_hover_enabled ? 'hidden' : '';
		itemHtml = itemHtml.replace(/{{itemHiddenClass}}/g, itemHiddenClass);

		var flipImageClass = boostPFSConfig.custom.secondary_image && images.length > 1 ? 'has-secondary-media-swap' : '';
		itemHtml = itemHtml.replace(/{{flipImageClass}}/ , flipImageClass)
		// Get image source
		var itemImageSrc = 'data-src="' + Utils.getFeaturedImage(images, '900x') + '" ';
		itemImageSrc += 'data-srcset=" ' + Utils.getFeaturedImage(images, '300x') + ' 300w,';
		itemImageSrc += Utils.getFeaturedImage(images, '400x') + ' 400w, ';
		itemImageSrc += Utils.getFeaturedImage(images, '500x') + ' 500w, ';
		itemImageSrc += Utils.getFeaturedImage(images, '600x') + ' 600w, ';
		itemImageSrc += Utils.getFeaturedImage(images, '700x') + ' 700w, ';
		itemImageSrc += Utils.getFeaturedImage(images, '800x') + ' 800w, ';
		itemImageSrc += Utils.getFeaturedImage(images, '900x') + ' 900w" ';
		// Get Thumbnail url
		var itemThumbUrl = Utils.getFeaturedImage(images, boostPFSConfig.custom.image_loading_style == 'blur-up' ? '50x' : '100x');
		// Get Flip image url
		var itemFlipImageUrl = images.length > 1 ? Utils.optimizeImage(images[1]['src']) : Utils.getFeaturedImage(images, '900x');
		
		// Add Thumbnail
		var product_set_width = data.featured_image.width;
		var align_height_value = '';
		if (boostPFSConfig.custom.align_height) {
			if (images.length > 0) {
				var collection_height = boostPFSConfig.custom.collection_height;
				var product_aspect_ratio = images[0]['width'] / images[0]['height'];
				product_set_width = product_aspect_ratio * collection_height;
				if (images[0]['width'] >= images[0]['height']) {
					align_height_value = 'width: 100%; height: auto;';
				} else {
					align_height_value = 'width: 100%;';
				}
			}
		}
		var backgroundColor = '';
		if (boostPFSConfig.custom.image_loading_style == 'color') {
			var dominantColorImage = Utils.getFeaturedImage(images, '1x');
			backgroundColor = 'background: url(' + dominantColorImage + ')';
		}
		var itemImagesHtml = '<div class="image__container">';
		itemImagesHtml += '<div class="image-element__wrap" style="width: ' + product_set_width + 'px; ' + backgroundColor + '">';
		itemImagesHtml += '<img src="' + itemThumbUrl + '"' +
							' alt="{{itemTitle}}" ' +
							' class="lazyload transition--' + boostPFSConfig.custom.image_loading_style + '"' +
							' style="' + align_height_value + 'max-width: ' + data.featured_image.width + 'px;"' +
							' data-sizes="auto" ' + itemImageSrc + '/>';
		itemImagesHtml += '</div>';
		itemImagesHtml += '</div>';
		// Add Flip Image
		if (boostPFSConfig.custom.secondary_image && images.length > 1) {
			// Get image source      
			var itemFlipImageSrc = 'data-src="' + Utils.optimizeImage(images[1]['src'], '900x') + '" ';
			itemFlipImageSrc += 'data-srcset=" ' + Utils.optimizeImage(images[1]['src'], '300x')  + ' 300w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '400x')  + ' 400w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '500x')  + ' 500w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '600x')  + ' 600w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '700x')  + ' 700w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '800x')  + ' 800w, ';
			itemFlipImageSrc += Utils.optimizeImage(images[1]['src'], '900x')  + ' 900w" ';
				itemImagesHtml += '<div class="image__container">';
				itemImagesHtml += '<div class="image-element__wrap" style="width:';
				if (images.length > 1) {
					itemImagesHtml += images[1]['width'] + 'px">';
				} else {
					itemImagesHtml += data.featured_image.width + 'px">';
				}
				itemImagesHtml += '<img src="' + itemFlipImageUrl + '" ';
				itemImagesHtml += 'class="lazyload transition--blur-up secondary lazypreload lazyautosizes secondary-media-hidden" alt="{{itemTitle}}" data-sizes="auto" ' + itemFlipImageSrc + '/>'
				itemImagesHtml += '</div>';
				itemImagesHtml += '</div>';
		}
	
		itemHtml = itemHtml.replace(/{{itemImages}}/g, itemImagesHtml);

		// Build Product Info when hovering 
		var itemProductInfoHoverHtml = '';
		if ((boostPFSConfig.custom.thumbnail_hover_enabled || boostPFSConfig.custom.quick_shop_enabled) && boostPFSConfig.custom.quick_shop_style == 'popup') {
			itemProductInfoHoverHtml = '<div class="thumbnail-overlay">' +
				'<a href="{{itemUrl}}" class="hidden-product-link">{{itemTitle}}</a>' +
				'<div class="info">' +
				(boostPFSConfig.custom.thumbnail_hover_enabled ? '{{itemProductInfo}}' : '') +
				'</div>' +
				'</div>';
		}
		itemHtml = itemHtml.replace(/{{itemProductInfoHover}}/g, itemProductInfoHoverHtml);

		// Build Product Info (product-info.liquid)
		var itemProductInfoHtml = '<div class="product-details">';
		itemProductInfoHtml += '<span class="title">{{itemTitle}}</span>';
		itemProductInfoHtml += '{{itemVendor}}';
		itemProductInfoHtml += '{{itemReview}}';
		itemProductInfoHtml += '{{itemPrice}}';
		itemProductInfoHtml += '</div>';
		itemHtml = itemHtml.replace(/{{itemProductInfo}}/g, itemProductInfoHtml);

		// Add Review badge
		var itemReviewHtml = '';
		if (boostPFSConfig.custom.enable_shopify_review_comments && boostPFSConfig.custom.enable_shopify_collection_badges) {
			itemReviewHtml = '<span class="shopify-product-reviews-badge" data-id="{{itemId}}"></span>';
		}
		itemHtml = itemHtml.replace(/{{itemReview}}/g, itemReviewHtml);

		// Add Vendor
		var itemVendorHtml = '';
		if (boostPFSConfig.custom.vendor_enable) {
			itemVendorHtml = '<span class="brand">' + data.vendor + '</span>';
		}
		itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

		// Add Price
		var itemPriceHtml = '';
		if (typeof comingsoonLabel[0] !== 'undefined') {
			itemPriceHtml += '<span class="modal_price">' + boostPFSConfig.label.coming_soon + '</span>';
		} else {
			itemPriceHtml += '<span class="price ' + onSaleClass + '">';
			if (!soldOut) {
				if (priceVaries && data.price_min > 0) {
					itemPriceHtml += '<small><em>' + boostPFSConfig.label.from_price + '</em></small> ';
				}
				if (data.price_min > 0) {
					itemPriceHtml += '<span class="money">' + Utils.formatMoney(data.price_min) + '</span>';
				} else {
					itemPriceHtml += boostPFSConfig.label.free_price;
				}
			} else {
				itemPriceHtml += '<span class="sold_out">' + boostPFSConfig.label.sold_out + '</span>';
			}
			if (onSale) {
				itemPriceHtml += ' <span class="was_price"><span class="money">' + Utils.formatMoney(data.compare_at_price_max) + '</span></span>';
			}
			itemPriceHtml += '</span>';
		}
		itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);

		// Add Swatches
		var itemColorSwatchesHtml = '';
		if (boostPFSConfig.custom.collection_swatches) {
			for (var k = 0; k < data.options.length; k++) {
				var option = data['options'][k];
				var downcasedOption = option.toLowerCase();
				var colorTypes = ['color', 'colour', 'farve'];
				if (colorTypes.indexOf(downcasedOption) > -1) {
					var option_index = k;
					var values = [];
					itemColorSwatchesHtml += '<div class="collection_swatches">';
					for (var i = 0; i < data.variants.length; i++) {
						var variant = data['variants'][i];
						var value = variant['options'][option_index];
						if (values.indexOf(value) == -1) {
							var values = values.join(',');
							values += ',' + value;
							values = values.split(',');
							var fileColorUrl = boostPFSConfig.general.asset_url.replace('boost-pfs-filter.js', Utils.slugify(value) + '.png');
							fileColorUrl = Utils.optimizeImage(fileColorUrl, '50x');
							itemColorSwatchesHtml += '<a href="' + Utils.buildProductItemUrl(data) + '?variant=' + variant.id + '" class="swatch" data-swatch-name="meta-' + downcasedOption + '_' + (value.replace(/\s/g, '_')).toLowerCase() + '">';
							itemColorSwatchesHtml += '<span ';
							if (boostPFSConfig.custom.products_per_row == 2) {
								itemColorSwatchesHtml += 'data-image="' + Utils.optimizeImage(variant.image, '600x') + '" ';
							} else if (boostPFSConfig.custom.products_per_row == 2) {
								itemColorSwatchesHtml += 'data-image="' + Utils.optimizeImage(variant.image, '500x') + '" ';
							} else {
								itemColorSwatchesHtml += 'data-image="' + Utils.optimizeImage(variant.image, '400x') + '" ';
							}
							itemColorSwatchesHtml += 'style="background-image: url(' + fileColorUrl + '); background-color: ' + Utils.slugify(value.split(' ').pop()) + ';">';
							itemColorSwatchesHtml += '</span>';
							itemColorSwatchesHtml += '</a>';
						}
					}
					itemColorSwatchesHtml += '</div>';
				}
			}
		}
		if (boostPFSConfig.custom.quick_shop_style == 'inline') {
			itemHtml = itemHtml.replace(/{{itemColorSwatchesInline}}/g, itemColorSwatchesHtml);
			itemHtml = itemHtml.replace(/{{itemColorSwatchesPopup}}/g, '');
		} else if (boostPFSConfig.custom.quick_shop_style == 'popup' && boostPFSConfig.custom.quick_shop_enabled) {
			itemHtml = itemHtml.replace(/{{itemColorSwatchesInline}}/g, '');
			itemHtml = itemHtml.replace(/{{itemColorSwatchesPopup}}/g, itemColorSwatchesHtml);
		} else {
			itemHtml = itemHtml.replace(/{{itemColorSwatchesInline}}/g, '');
			itemHtml = itemHtml.replace(/{{itemColorSwatchesPopup}}/g, '');
		}

		// Add main attribute
		itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
		itemHtml = itemHtml.replace(/{{itemHandle}}/g, data.handle);
		itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
		itemHtml = itemHtml.replace(/{{itemUrl}}/g, Utils.buildProductItemUrl(data));

		return itemHtml;
	};

	// Build advanced class
	function buildItemCollectionGroupThumbClass(index, productsPerRow) {
		var temp = index < productsPerRow ? index : index % productsPerRow;
		if (temp == 0) {
			return 'omega';
		} else if (temp == 1) {
			return 'alpha';
		}
		return '';
	}

	// Build Pagination
	ProductPaginationDefault.prototype.compileTemplate = function (totalProduct) {
		// Get page info
      	if (!totalProduct) totalProduct = this.totalProduct;
		var currentPage = parseInt(Globals.queryParams.page);
		var totalPage = Math.ceil(totalProduct / Globals.queryParams.limit);

		if (Settings.getSettingValue('general.paginationType') == 'default' && totalPage > 1) {
			var paginationHtml = boostPFSTemplate.paginateHtml;

			// Build Previous
			var previousHtml = (currentPage > 1) ? boostPFSTemplate.previousHtml : '';
			previousHtml = previousHtml.replace(/{{itemUrl}}/g, Utils.buildToolbarLink('page', currentPage, currentPage - 1));
			paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);

			// Build Next
			var nextHtml = (currentPage < totalPage) ? boostPFSTemplate.nextHtml : '';
			nextHtml = nextHtml.replace(/{{itemUrl}}/g, Utils.buildToolbarLink('page', currentPage, currentPage + 1));
			paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);

			// Create page items array
			var beforeCurrentPageArr = [];
			for (var iBefore = currentPage - 1; iBefore > currentPage - 3 && iBefore > 0; iBefore--) {
				beforeCurrentPageArr.unshift(iBefore);
			}
			if (currentPage - 4 > 0) {
				beforeCurrentPageArr.unshift('...');
			}
			if (currentPage - 4 >= 0) {
				beforeCurrentPageArr.unshift(1);
			}
			beforeCurrentPageArr.push(currentPage);

			var afterCurrentPageArr = [];
			for (var iAfter = currentPage + 1; iAfter < currentPage + 3 && iAfter <= totalPage; iAfter++) {
				afterCurrentPageArr.push(iAfter);
			}
			if (currentPage + 3 < totalPage) {
				afterCurrentPageArr.push('...');
			}
			if (currentPage + 3 <= totalPage) {
				afterCurrentPageArr.push(totalPage);
			}

			// Build page items
			var pageItemsHtml = '';
			var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
			for (var iPage = 0; iPage < pageArr.length; iPage++) {
				if (pageArr[iPage] == '...') {
					pageItemsHtml += boostPFSTemplate.pageItemRemainHtml;
				} else {
					pageItemsHtml += (pageArr[iPage] == currentPage) ? boostPFSTemplate.pageItemSelectedHtml : boostPFSTemplate.pageItemHtml;
				}
				pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
				pageItemsHtml = pageItemsHtml.replace(/{{itemUrl}}/g, Utils.buildToolbarLink('page', currentPage, pageArr[iPage]));
			}
			paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

			return paginationHtml;
		}
		return '';
	};

	// Build Sorting
	ProductSorting.prototype.compileTemplate = function () {
		if (boostPFSTemplate.hasOwnProperty('sortingHtml')) {

			var sortingArr = Utils.getSortingList();
			if (sortingArr) {
				// Build content 
				var sortingItemsHtml = '';
				for (var k in sortingArr) {
					sortingItemsHtml += '<option value="' + k + '">' + sortingArr[k] + '</option>';
				}
				var html = boostPFSTemplate.sortingHtml.replace(/{{sortingItems}}/g, sortingItemsHtml);

				return html;
			}
		}
		return '';
	};
  
	// Sorting render
	ProductSorting.prototype.render = function () {
		jQ(Selector.topSorting).html(this.compileTemplate());
		var $selectInput = jQ(Selector.topSorting);
		if ($selectInput.length) {
		$selectInput.val(Globals.queryParams.sort);
		}
	}

	// Build Sorting event
	ProductSorting.prototype.bindEvents = function () {
		var _this = this;
		jQ(Selector.topSorting).change(function (e) {
			// Append "sort" param to url
			// Used to Scroll to the previous position after returning from Product page
			FilterApi.setParam('sort', jQ(this).val());
			FilterApi.applyFilter('sort');
		})
	};

	// Build Breadcrumb
	Breadcrumb.prototype.compileTemplate = function (colData, apiData) {
		if (!colData) colData = this.collectionData;
		if (typeof colData !== 'undefined' && colData.hasOwnProperty('collection')) {
			var colInfo = colData.collection;

			var breadcrumbHtml = '<span ><a href="' + boostPFSConfig.shop.url + '" title="' + boostPFSConfig.shop.name + '" class="breadcrumb_link"><span>' + boostPFSConfig.label.breadcrumb_home + '</span></a></span> ';
			breadcrumbHtml += '<span class="breadcrumb-divider">/</span> ' +
				'<span><a href="{{currentCollectionLink}}" title="{{currentCollectionTitle}}" class="breadcrumb_link"><span>{{currentCollectionTitle}}</span></a></span> ';
			// Build Tags
			var currentTagsHtml = '';
			if (Array.isArray(boostPFSConfig.general.current_tags)) {
				var current_tags = boostPFSConfig.general.current_tags;
				for (var k = 0; k < current_tags.length; k++) {
					var tag = current_tags[k];
					currentTagsHtml += '<span class="breadcrumb-divider">/</span>';
					currentTagsHtml += '<span><a href="/collections/{{currentCollectionLink}}/' + this.slugify(tag) + '" title="' + tag + '"><span>' + tag + '</span></a></span>';
				}
			}
			breadcrumbHtml += currentTagsHtml;
			// Build Collection Info
			breadcrumbHtml = breadcrumbHtml.replace(/{{currentCollectionLink}}/g, '/collections/' + colInfo.handle).replace(/{{currentCollectionTitle}}/g, colInfo.title);
			// Top Pagination
			breadcrumbHtml += ' <span class="boost-pfs-filter-top-pagination"></span>';
			return breadcrumbHtml;
		}
		return '';
	};

	// Call Theme function to Build additional elements for Product list
	ProductList.prototype.afterRender = function (data) {
		// Build Quick view data
		if (!data) data = this.data;
		if (boostPFSConfig.custom.quick_shop_enabled) {
			this.buildExtrasProductListByAjax(data, 'boost-pfs-quickview', function(results) {
				results.forEach(function(result) {
					var element = '';
					if (boostPFSConfig.custom.quick_shop_style == 'inline') {
						element = jQ('[data-boost-theme-quickview="'+ result.id +'"]');
						if (element.find('.js-quick-shop-link').length == 0) {
							element.append(result.quickview);
						}
					} else {
						element = jQ('[data-boost-theme-quickview="'+ result.id +'"] div.info');
						if (element.find('.js-quick-shop-link').length == 0) {
							element.append(result.quickview);
						}
					}
				});
				buildTheme();
			});
		}
	};

	// Build Additional elements
	FilterResult.prototype.afterRender = function (data) {
		if (!data) data = this.data;
		// Remove InstantClick
		jQ(Selector.filterTree).find('a').attr('data-no-instant', '');

		// Remove product wrapper
		if (jQ(Selector.products).children().hasClass('product-list')) {
			jQ(Selector.products).children().children().unwrap();
		}

		// Build top pagination
		var totalPage = Math.ceil(data.total_product / Globals.queryParams.limit);
		var topPaginationHtml = '<span class="breadcrumb-divider">/</span> ' + (boostPFSConfig.label.breadcrumb_page).replace(/{{ current_page }}/g, Globals.queryParams.page).replace(/{{ pages }}/g, totalPage);
		jQ('.boost-pfs-filter-top-pagination').html(topPaginationHtml);
		jQ(".load-more__icon").remove();

		buildTheme();
	};

	function buildTheme() {
		if ((Shopify.theme_settings.enable_shopify_review_comments || Shopify.theme_settings.enable_shopify_collection_badges) && window.SPR) {
			SPR.registerCallbacks();
			SPR.initRatingHandler();
			SPR.initDomEls();
			SPR.loadProducts();
			SPR.loadBadges();
		}
		if (Shopify.theme_settings.show_multiple_currencies) {
			convertCurrencies();
		}
		imageFunctions.showSecondaryImage()
		Shopify.PaymentButton.init();
		productPage.init();
		if (Shopify.theme_settings.quick_shop_enabled) {
			quickShop.init();
		}
		hideNoScript();
	}

})(); // Add this at the end






/* Begin patch boost-010 run 2 */
Filter.prototype.beforeInit=function(){var t=this.isBadUrl();t&&(this.isInit=!0,window.location.href=window.location.pathname)},Filter.prototype.isBadUrl=function(){try{var t=decodeURIComponent(window.location.search).split("&"),e=!1;if(t.length>0)for(var n=0;n<t.length;n++){var i=t[n],r=(i.match(/</g)||[]).length,a=(i.match(/>/g)||[]).length,o=(i.match(/alert\(/g)||[]).length,h=(i.match(/execCommand/g)||[]).length;if(r>0&&a>0||r>1||a>1||o||h){e=!0;break}}return e}catch(l){return!0}};
/* End patch boost-010 run 2 */
