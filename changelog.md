# Changelog

Date format is year-day-month

## 2021-22-03
- Numerous minor snags

## 2021-19-03
- Installed Fonts
- Typography according to new design
- Announcement Bar Size
- SNAPPPT installed
- Minor style changes
- Collection Banner with Title outside of image
- Collection Banner now has default fallback image
- Benetti Block (flickity-collection-side) improved

## 2021-16-03
- Installed Omnisend

## 2021-15-03
- Rearranged stylesheets
- Removed a lot of colors from stylesheets
- Applied new brand colours
- Fixed margins on pages
- Logo served in full resolution

## 2021-26-01
- Another fix on Minicart behaviour. .blocked-scroll no longer uses pointer-events: none
- Minicart shows a number on mobile, and it can be clicked thorugh
- Installed Kiwi Size Guide App

## 2021-26-01
- Minicart behaviour should be fixed. Now clicking add to cart scrolls to the top of the page, after opening the cart the screen is locked, but clicking on continue shopping or on the cart removes the minicart screen.
On desktop it is enough to mouse out of the minicart to make it disappear. All the behaviours should be as originally intended, with some minor glitches that show up very rarely.
- Now preparing to install Kiwi size guide app.

## 2021-26-01
- Attempting to fix the minicart behaviour

## 2021-26-01
- Various minor graphical snags addressed (alignments on headers and products, colors)
- Attempted to fix the minicart on mobile. It wouldn't open again once closed, and the "continue shopping" link was not doing its job
- Now the fadeout function of the minicart seems to be broken, but for the rest it is mostly functional.

## 2021-26-01
- Removed usps custom element
- Properly installed the CSS variables snippet
- Replaced most colours with CSS variables, to be able to edit them in customiser

## 2021-25-01
- Customised 'include-image-with-text-overlay.liquid' so an alternate mobile image can be provided. 
- Removed "Select Options" from hovered products on the collection page.

## 2021-25-01
- Fixed blocked scroll on mobile when clicking on "add to cart"
- Developed Sidebar Content feature (Image with link), it can now be added from the customiser
- Customised collection-list.liquid so that the last collection can be hidden on mobile, 
allowing client to have an odd number of collections on homepage, and an even number on mobile.
- Changed options border for size choices and colour choices. It's now consistent. 

## 2021-22-01
- Styled footer for mobile
- Added contact page
- Added USP section in all pages
- Styled buttons on minicart and cart page. Made it easier to hover over the minicart. Popup seems to be functional, cart seems to work with products added.
- Added USP bar to every page by default. Removed from homepage customiser, it's now a global section in theme.liquid.
- Moved text and button on "featured promotions" mobile version so they would sit on the image. Might be customiser only.
- Loads of minor fixes from the snags (basically all of the snags but one)
- Fixed Terms of Service and Privacy Policy pages to have their content centered.

## 2021-20-01
- Fixed Wishlist not working on homepage.
- Reversed and optimised CSS logic for searchbar, to make it display instantly on mobile, and very quickly on desktop.
- Achieved instant loading time for wishlist icon by replacing font-icon with inline SVG. Shopify-UI-library component already had SVG loading but it couldn't render. Can't find cause yet.
- Adjusted behaviours and thickness of navigation SVG icons, on hover and not, adjusted color of search icon for mobile to match the rest.
- Added images to Megamenu to match experimental designs.
- Added currency converter in footer credits
- Corrected styling of footer credits

## 2021-19-01
- Fixed pagination backgrounds and border radius
- Added custom USP section to product page
- Styled custom USP section
- Changed header to match usual format on mobile
- Removed login icon on mobile, added it as a menu item instead.

## 2021-19-01
- Installed Wishlist Plus
- Integrated Wishlist with Product Filter & Search app
- Added icon in homepage
- Fixed styling for custom_link icons in homepage, desktop & mobile
- Added wishlist button on product page
- Styled buttons on product page

## 2021-19-01
- Styled Header for mobile, fixed bugs on header.

## 2021-18-01
- Styled Homepage (mobile)
- Installed sf9-header shopify-ui-library component
- Styled header for desktop. Added colors on hover for menu elements and custom_html svg icons.

## 2021-18-01
- Styled Footer

## 2021-15-01
- Fixed and adjusted usp-bar, now fully functional
- Developed new custom section with slider of products, with custom shadows and sidebar with title, description and collection link button
- Styled new section, added shadows and more contained dimensions of product image. Limited text height minimum for design consistency (gets all images to be in line height-wise)
- Changed footer to have 6 columns.

## 2021-14-01
- Installed shopify-ui-library/usp-bar

## 2021-14-01
- Set up first half of Homepage for Desktop. Unsure on button names (menswear, womenswear for now), unsure on design for mobile version. Still waiting on client assets.
- Populated homepage up to "black friday sales event".
- Setup typography and colours as much as possible on customiser.
- Fixed up wording in newsletter (locales/en.default.json)
- Styled all elements in Homepage that exist so far (everything except slider element and USP).
- Removed details on hover from collection list. Restored their appearance outside of the image by customising template.
- Added an underline animation on hover to the collection list.
- installed shopify-ui-library/ui-component-usps