let config = require('./config.json');
try {
    const localConfig = require('./config.local.json');
    config = Object.assign(config, localConfig);
} catch (ignored) {}
const gulp = require('gulp');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const themeKit = require('@shopify/themekit');
const argv = require('yargs').argv;
const open = require('gulp-open');
const webpack = require('webpack');
const webpackConfigDev = require('./webpack.config.dev');
const webpackConfigProd = require('./webpack.config.prod');
const tap = require('gulp-tap');
const path = require('path');
const replace = require('gulp-replace');
const svgo = require('gulp-svgo');
const rename = require("gulp-rename");

let env = process.env.NODE_ENV || 'development';
const src = './src';

gulp.task('set-dev-env', function(cb) {
   env = 'development';
   cb();
});

gulp.task('set-prod-env', function(cb) {
   env = 'production';
   cb();
});

gulp.task('scss', function() {
    return gulp.src([
        './assets/styles.scss.liquid',
        `${src}/scss/mixins/**/*.scss.liquid`,
        `${src}/scss/variables/**/*.scss.liquid`,
        `${src}/scss/base/**/*.scss.liquid`,
        `${src}/scss/components/**/*.scss.liquid`,
        `${src}/scss/sections/**/*.scss.liquid`,
        `${src}/scss/pages/**/*.scss.liquid`,
        `${src}/scss/variables/**/*.scss.liquid`,
        `${src}/scss/vendor/**/*.scss.liquid`,
        `${src}/scss/utilities/**/*.scss.liquid`])
        .pipe(concat('styles-customised.scss.liquid'))
        .pipe(gulp.dest('./assets'));
});

gulp.task('js', function() {
    const webpackConfig = env === 'development' ? webpackConfigDev : webpackConfigProd;
    return new Promise((resolve, reject) => {
       webpack(webpackConfig, (error, stats) => {
           if (error) {
               return reject(error);
           }
           if (stats.hasErrors()) {
               return reject(new Error(stats.compilation.errors.join('\n')));
           }
           resolve();
       });
    });
});

gulp.task('svgs', function() {
    return gulp.src('src/svgs/*')
        .pipe(svgo({plugins:[{removeViewBox:false}]}))
        .pipe(rename(function (path) {
            return {
                dirname: path.dirname,
                basename: 'icon-' + path.basename + '.svg',
                extname: '.liquid'
            };
        }))
        .pipe(gulp.dest('snippets/'));
});

const templates = [
    '**/*.liquid',
    '!./assets/**/*',
    '!./node_modules/**/*',
    `!${src}/**/*`,
    '!./snippets/customer-has-tag.liquid',
    '!./snippets/customer-is-wholesale.liquid',
    '!./snippets/product-properties.liquid',
    '!./snippets/product-property.liquid',
    '!./snippets/product-has-tag.liquid',
    '!./snippets/titlecase.liquid',
    '!./snippets/cart-invalid-quantity.liquid'
];

gulp.task('add-template-name', function() {
    return gulp.src(templates)
        .pipe(tap(function(file) {
            const filePath = path.relative(path.resolve('./'), file.path);
            const regex = /^<!-- Template:/;
            if (!regex.test(file.contents)) {
                file.contents = Buffer.concat([
                    new Buffer.from(`<!-- Template: ${filePath} -->\n`), file.contents
                ]);
            }
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('remove-template-name', function() {
    return gulp.src(templates)
        .pipe(replace(/<!-- Template: .+ -->\n/, ''))
        .pipe(gulp.dest('./'));
});

gulp.task('watch-dev', gulp.series(['set-dev-env', function() {
    gulp.watch(`${src}/scss/**/*.scss.liquid`, gulp.series('scss'));
    gulp.watch(`${src}/js/**/*.js`, gulp.series('js'));
    gulp.watch(`${src}/svgs/*.svg`, gulp.series('svgs'));

    themeKit.command('watch', {
        notify: '/tmp/theme.update',
        env: 'development',
        vars: 'themekit-vars'
    });

    browserSync.init({
        proxy: `${config.storeUrl}${config.path}?preview_theme_id=${config.themeId.development}`,
        reloadDelay: 3000
    });

    gulp.watch('/tmp/theme.update').on('change', browserSync.reload);
}]));

gulp.task('watch-production', gulp.series(['set-prod-env', function() {
    gulp.watch(`${src}/scss/**/*.scss.liquid`, gulp.series('scss'));
    gulp.watch(`${src}/js/**/*.js`, gulp.series('js'));
    gulp.watch(`${src}/svgs/*.svg`, gulp.series('svgs'));

    themeKit.command('watch', {
        notify: '/tmp/theme.update',
        env: 'production',
        vars: 'themekit-vars'
    });

    browserSync.init({
        proxy: `${config.storeUrl}${config.path}?preview_theme_id=${config.themeId.production}`,
        reloadDelay: 3000
    });

    gulp.watch('/tmp/theme.update').on('change', browserSync.reload);
}]));

gulp.task('deploy', function() {
    return themeKit.command('deploy', {
        env: env,
        vars: 'themekit-vars'
    });
});

gulp.task('deploy-dev', gulp.series(['set-dev-env', 'scss', 'js', 'deploy']));
gulp.task('deploy-prod', gulp.series(['set-prod-env', 'scss', 'js', 'deploy']));
gulp.task('deploy-both', gulp.series('deploy-dev', 'deploy-prod'));

gulp.task('download', function() {
    return themeKit.command('download', {
        env: env,
        vars: 'themekit-vars'
    });
});

gulp.task('download-dev', gulp.series(['set-dev-env', 'download']));
gulp.task('download-prod', gulp.series(['set-prod-env', 'download']));

gulp.task('download-theme', gulp.series(['set-dev-env', function(cb) {
    if (argv.themeid == null) {
        console.log('Argument --themeid is required');
        cb();
    } else {
        return themeKit.command('deploy', {
            env: env,
            vars: 'themekit-vars',
            themeid: argv.themeid
        });
    }
}]));

gulp.task('preview', function() {
   const themeId = config.themeId[env];
    gulp.src(__filename)
        .pipe(open({uri: `${config.storeUrl}${config.path}?preview_theme_id=${themeId}`}))
});

gulp.task('preview-dev', gulp.series(['set-dev-env', 'preview']));
gulp.task('preview-prod', gulp.series(['set-prod-env', 'preview']));

gulp.task('default', gulp.series('watch-dev'));


gulp.task('sf9-theme-changes', () => {
    return gulp.src('layout/*.liquid')
     .pipe(replace(/styles.scss.css/, 'styles-customised.scss.css'))
     .pipe(replace(/{{ content_for_header }}/, '{% if false %}\n {{ content_for_header }}\n {% endif %}\n {{ content_for_header | replace: "<body onload=\'document._boomrl\(\);\'>", "<bodx onload=\'document._boomrl\(\);\'>" }}'))
     .pipe(replace(/{%- if settings.show_multiple_currencies/, '<script src="{{ \'custom.js\' | asset_url }}"></script>\n\n {%- if settings.show_multiple_currencies'))
     .pipe(gulp.dest('layout/'));
  });
  gulp.task('create-src', () => { 
    return gulp.src('*.*', {read: false})
      .pipe(gulp.dest('./src'))
      .pipe(gulp.dest('./src/js'))
      .pipe(gulp.dest('./src/js/index.js'))
      .pipe(gulp.dest('./src/scss'));
  });
  gulp.task('theme-setup', gulp.series('sf9-theme-changes', 'create-src'));